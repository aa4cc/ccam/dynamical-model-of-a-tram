%% Constant parameters of the tram
r = 0.325;  % Wheel radius
k_pos = 1449;   % Traction characteristics: positive notch 
k_neg = 1176;   % Traction characteristics: negative notch
max_pow = 44000*4;  % Max power given by sum of all (four) motors
num_of_wheels = 8;  % Number of wheels.
num_of_tr_wheels = 8;   % Number of traction wheels.
m_wheel = 195;  % Weight of one wheel.
J = 1/2*m_wheel*r^2;    % Moment of inertia of one wheel.
g = 9.81;   % Gravitional constant