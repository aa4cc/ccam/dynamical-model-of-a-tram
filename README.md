# Related repositories:
- https://gitlab.fel.cvut.cz/aa4cc/vset/tram
- https://gitlab.fel.cvut.cz/aa4cc/vset/tram/-/tree/master/parse_osm_tram_track


# The model and related work is described in the Master's thesis and a paper:
- https://gitlab.fel.cvut.cz/aa4cc/vset/reports/master-thesis-by-loi-do
- https://www.sciencedirect.com/science/article/pii/S2405896320326409?via%3Dihub

# Original repository of the "Parse OSM (open-street maps)"
- https://github.com/johnyf/openstreetmap


# Running examples:
- Clone the repository and open the root folder in Matlab
- Run the script: add_all_folders_to_path.m (adds temporarily all folders and subfolders into the Matlab path)
- Run the demos located in the folder: .\demo_sim_tram\ 
- Note: when running demos, do not leave the root folder. All paths in scripts are relative to the root folder.