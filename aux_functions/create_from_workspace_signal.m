function out_signal = create_from_workspace_signal(change_vals, change_times, sim_Ts, sim_time)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

change_idx = change_times./sim_Ts + 1; % +1 because of indexing from 1

data = zeros(1, sim_time/sim_Ts + 1); % +1 for time zero

for ii=1:numel(change_vals)
    data(change_idx(ii):change_idx(ii+1)) = change_vals(ii);
end

out_signal.time = (0:sim_Ts:sim_time)';
out_signal.signals.values = data';
out_signal.signals.dimensions = 1;
end

