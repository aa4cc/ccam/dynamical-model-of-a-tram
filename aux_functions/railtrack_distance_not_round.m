function [distance] = railtrack_distance_not_round(railtrack,point_s,point_e)
%railtrack_distance Compute railtrack distance of two points
%   (distance from point1 to point2 by railtrack)
%   point_s: start
%   point_e: end

%   Project points on railtrack
[i1, p1] = project_point_on_railtrack(point_s, railtrack);
[i2, p2] = project_point_on_railtrack(point_e, railtrack);

track_size = numel(railtrack.ids);
distance = 0;

if (i1 == 1 && i2 == 32)
    disp('');
end

s1 = railtrack.coords(:,i1);
s2 = railtrack.coords(:,i2);

if (i1 == i2)
    % Points are on the same segment
    distance = LLA_points_distance(s2,p2) - LLA_points_distance(s1,p1);
else
    forward_curr_p = p1; % go from p1 to p2 forward in index of tram_track
    forward_distance = 0;
    forward_indx = i1;
    % --------------------------------
%     backward_distance = 0;
    backward_indx = i1;
    backward_distance = LLA_points_distance(p1, s1); % We can already know this because i1 ~= i2
    distance_found  = false;
    
    while (~distance_found)
        if forward_indx ~= i2 % we are travelling from p1 to p2 forward in index
            forward_next_indx = mod(forward_indx,track_size)+1;
            forward_next_p = railtrack.coords(:,forward_next_indx);
            forward_distance = forward_distance + LLA_points_distance(forward_curr_p,forward_next_p);
            forward_curr_p = forward_next_p;
            forward_indx = forward_next_indx;
        else
            forward_distance = forward_distance + LLA_points_distance(forward_curr_p,p2);
            distance = forward_distance;
            distance_found = true;
        end
        
        % ------------- Backwards -------------
        backward_next_p = railtrack.coords(:,backward_indx); 
            % Next point is start of the segment on which we were in last iteration
            % At the beginning, it was start of the segment on which p1 was
            
        
        if backward_indx == mod(i2,track_size)+1 
            % If point p2 lies of the segment before current segment
            backward_distance = backward_distance + LLA_points_distance(p2,backward_next_p);
            distance = -backward_distance; % We went backwards
            distance_found = true;
        else 
            if backward_indx ~= 1
                backward_curr_p = railtrack.coords(:,backward_indx);
                backward_indx = backward_indx - 1;
                backward_next_p = railtrack.coords(:,backward_indx);
                backward_distance = backward_distance + LLA_points_distance(backward_curr_p,backward_next_p);
            end
        end
    end
end
