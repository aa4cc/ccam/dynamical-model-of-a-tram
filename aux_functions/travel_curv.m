function curvature = travel_curv(gen_traj, tram_track, distance_ahead, distane_backwards)
%FUNCTION START: travel_curv: compute curvation of tram track given
%position_data (trajectory)

data_mat = cell2mat(gen_traj);
time = data_mat(1,:);
GPS_lla_points = data_mat(2:3,:);
curr_point = GPS_lla_points(:,1);

num_of_samples = size(time,2);

curvature = zeros(1,num_of_samples);
for i=1:num_of_samples
    [next_i, ~] = next_railtrack_point(tram_track, curr_point, distance_ahead);
    [prev_i, ~] = prev_railtrack_point(tram_track, curr_point, distane_backwards);
    curvature(i) = delta_angle(tram_track, prev_i, next_i, distane_backwards, distance_ahead);
    
    % Next iter;
    curr_point = GPS_lla_points(:,i);
end

%FUNCTION END
end

