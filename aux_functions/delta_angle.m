function curvature = delta_angle(tram_track,index_start, index_end, distane_backwards, distance_ahead)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

relative_angles = zeros(1, index_end-index_start);
% distances = zeros(1, index_end-index_start+1); % +1 for distance of another segment
% distances(1) = tram_track{4,1}(index_start);

track_index = index_start:index_end;
for i=1:size(track_index,2)
    relative_angles(i) = tram_track.angles(track_index(i));
%     distances(i+1) = tram_track{4,1}(track_index(i)+1);
%     radii(i) = (distances(i)+distances(i+1))/(2*relative_angles(i));
end

% distances(index_end-index_start+1) = tram_track{4,1}(index_end+1);
end_angle = sum(relative_angles);
% radius = mean(radii);

% radii = zeros(1, index_end-index_start);


% end_angle = sum(relative_angles);
% final_angle = 2*pi - center_angle;

radius = (distane_backwards+distance_ahead)/end_angle;
curvature = 1/radius;
% d_angle = end_angle/length;

