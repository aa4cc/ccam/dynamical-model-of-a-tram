function [gps_pos_data, gps_vel_data, acc_data_x, acc_data_y, gyro_z_out] = gen_sensor_data_v2_on_track(start_LLA, state, Ts, tram_track)
%gen_sensor_data_on_track Summary of this function goes here
%   Detailed explanation goes here
%   Add gyroscope and acc_y

gps_std = sqrt(25); % Covariance of the GPS measurement
h = 220; % Altitude, assume constant. Only to add noise to GPS measurements: change LLA 

% num_of_p = numel(state.dist);
distances = state.dist;
indx = 1:Ts/state.Ts:numel(distances);
num_of_p = numel(indx);

%% GPS pos data
trajectory_LLA = zeros(2,num_of_p);
[~, trajectory_LLA(:,1)] = project_point_on_railtrack(start_LLA, tram_track);

% indx = 1:Ts/state.Ts:numel(distances);
for ii=2:num_of_p
    [~, trajectory_LLA(:,ii)] = next_railtrack_point(tram_track, trajectory_LLA(:,1), distances(indx(ii)));
    % Make GPS noisy
    angle = 2*pi.*rand(1,1); % Generate angle, random, uniformly
    r = gps_std*randn(1,1); % Generate distance, 
    dx = r*cos(angle);
    if (angle >= pi)
        dy = -sqrt(r^2 - dx.^2);
    else
        dy = sqrt(r^2 - dx.^2);
    end
    
    [dlat_rad,dlon_rad]=lg2dg(dx,dy,deg2rad(trajectory_LLA(2,ii)),h);
    dlon = rad2deg(dlon_rad);
    dlat = rad2deg(dlat_rad);
    trajectory_LLA(:,ii) = trajectory_LLA(:,ii) + [dlon;dlat];
end

gps_pos_data.data = trajectory_LLA;
gps_pos_data.time = state.time(1:Ts/state.Ts:end);

%% ACC data
% Constants for noises
std_noise_velocity_dependent = 0.008842;
std_white_noise_std = 0.015;
amplitude_const_noise = 0.117;
Fconst = 56;
freq_vel_depend_first = 288/11;


noise_const = amplitude_const_noise*sin(2*pi*Fconst*state.time).*sign(state.vel);
noise_white = (std_white_noise_std + std_noise_velocity_dependent*state.vel).*randn(size(state.time));

phi_sin_1 = 2*pi*(freq_vel_depend_first*(state.dist));
phi_sin_2 = 2*pi*(2*freq_vel_depend_first*(state.dist));
phi_sin_3 = 2*pi*(4*freq_vel_depend_first*(state.dist));
noise_speed_dep_1 = 0.06.*sin(phi_sin_1).*sign(state.vel);
noise_speed_dep_2 = 0.06.*sin(phi_sin_2).*sign(state.vel);
noise_speed_dep_3 = 0.06.*sin(phi_sin_3).*sign(state.vel);

acc_out_noise = state.acc + noise_const + noise_white + noise_speed_dep_1 + noise_speed_dep_2 + noise_speed_dep_3;

acc_data_x.data = acc_out_noise(1:Ts/state.Ts:end)';
acc_data_x.time = state.time(1:Ts/state.Ts:end);

%% ACC y
acc_y_out_noise = state.acc_y + noise_const + noise_white + noise_speed_dep_1 + noise_speed_dep_2 + noise_speed_dep_3;

acc_data_y.data = acc_y_out_noise(1:Ts/state.Ts:end)';
acc_data_y.time = acc_data_x.time;

%% Gyro z
gyro_z_out_noise = state.gyro_z + 0.02*(noise_const + noise_white + noise_speed_dep_1 + noise_speed_dep_2 + noise_speed_dep_3);
gyro_z_out.data = gyro_z_out_noise(1:Ts/state.Ts:end)';
gyro_z_out.time = acc_data_x.time;


%% GPS velocity data
% gps_vel_data.data = state.vel(1:Ts/state.Ts:end)' + 0.5*randn(1,num_of_p);
error_vel = cumsum(randn(1,num_of_p));
error_vel = 0.5*error_vel/max(abs(error_vel));
gps_vel_data.data = max(state.vel(1:Ts/state.Ts:end)' + error_vel, 0);
gps_vel_data.time = acc_data_x.time;

end

