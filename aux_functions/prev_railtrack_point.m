function [index,end_point] = prev_railtrack_point(railtrack,start_point,distance)
%prev_railtrack_point: Finds next point on tram trail given by starting
%point (LLA) and traveled distance (metres)
%   Detailed explanation goes here

% get projection of start point on track and get index of starting node
[index, p_start] = project_point_on_railtrack(start_point, railtrack);

found = false;
temp_dist = 0;
curr_point = p_start;

% Find start node of segment, on which end_point lies
while (~found)
    % compute distance to end of current segment
    prev_distance = temp_dist;
    
    %     if (index ~= 1) % if tram is not at the beginning of the track
    prev_point = railtrack.coords(:,index);
    temp_dist = temp_dist + LLA_points_distance(prev_point,curr_point);
    
    if (temp_dist < distance)
        % next iter init
        curr_point = prev_point;
        index = index - 1;
        if (index == 0) 
            index = numel(railtrack.ids);
        end
    else
        % Exceed distance, final point is between "curr_point" and "prev_point"
        found = true;
        % Compute how much to travel from curr_point to end_point;
        dist_last_segment = distance - prev_distance;
        last_segment_dist = LLA_points_distance(curr_point, prev_point);
        ratio = dist_last_segment/last_segment_dist;
        end_point = prev_point + (1-ratio)*(curr_point - prev_point);
    end
end




end

