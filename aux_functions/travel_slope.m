function sin_slope_filt = travel_slope(trajectory, tram_track, distance_ahead, distance_backwards)
%FUNCTION START: travel_curv: compute vertical curvation of the given tram track

data_mat = cell2mat(trajectory);
time = data_mat(1,:);
GPS_lla_points = data_mat(2:3,:);
curr_point = GPS_lla_points(:,1);

num_of_samples = size(time,2);

sin_slope_filt = zeros(1,num_of_samples);
for i=1:num_of_samples
    [next_i, ~] = next_railtrack_point(tram_track, curr_point, distance_ahead);
    [prev_i, ~] = prev_railtrack_point(tram_track, curr_point, distance_backwards);
    
    %%
    sin_theta = zeros(1, next_i-prev_i);
    track_index = prev_i:next_i;
    for j=1:size(track_index,2)
        sin_theta(j) = (tram_track.altitude(track_index(j)+1) - tram_track.altitude(track_index(j)))/tram_track.distances(track_index(j));
    end
%     end_altitude_change = sum(relative_change_of_theta);
    sin_slope_filt(i) = mean(sin_theta);
    %%

    
    % Next iter;
    curr_point = GPS_lla_points(:,i);
end

%FUNCTION END
end

