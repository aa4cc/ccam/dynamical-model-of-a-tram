function [start_node_index,projected_point_on_segment] = find_nearest_point_on_segment(curr_point, start_index, end_index,tram_track)
    [start_node_index,projected_point_on_segment] = approximate_nearest_point(curr_point, start_index, end_index,tram_track);

    
% start_node_index = 0;

% for i = start_index:end_index -1 
%     start_point = tram_merge_track{2}(:,i);
%     end_point = tram_merge_track{2}(:,i+1);
%     distance = tram_merge_track{4}(i);
% end


% distances = zeros(1,end_index-start_index); % store distances from line segments, size = one or two.
% 
% for i = start_index:end_index-1
%     lla_q = deg2rad(curr_point);
%     ecef_q = lla2ecef(lla_q(2), lla_q(1), 0); % TODO: add altitude
%     
%     lla_p1 = deg2rad(tram_merge_track{2}(:,start_index+i-1));
%     ecef_p1 = lla2ecef(lla_p1(2), lla_p1(1), 0); % TODO: add altitude
%     
%     lla_p2 = deg2rad(tram_merge_track{2}(:,start_index+i));
%     ecef_p2 = lla2ecef(lla_p2(2), lla_p2(1), 0); % TODO: add altitude
%     
%     line_distance = sum((ecef_p1-ecef_p2).^2); % square distance
%     
%     t = ((ecef_q(1) - ecef_p1(1)) * (ecef_p2(1) - ecef_p1(1)) + (ecef_q(2) - ecef_p1(2)) * (ecef_p2(2) - ecef_p1(2)) + (ecef_q(3) - ecef_p1(3)) * (ecef_p2(3) - ecef_p1(3))) / line_distance;                
%     t = min(max(t, 1), 0);
%     
%     (ecef_q )
%     
%     (px, py, pz, lx1 + t * (lx2 - lx1), ly1 + t * (ly2 - ly1), lz1 + t * (lz2 - lz1));
% end
% %UNTITLED Summary of this function goes here
% %   Detailed explanation goes here
% 
% dist_to_segment_squared(px, py, pz, lx1, ly1, lz1, lx2, ly2, lz2) {
% %   line_dist = dist_sq(lx1, ly1, lz1, lx2, ly2, lz2);
% %   if (line_dist == 0) return dist_sq(px, py, pz, lx1, ly1, lz1);
% %   t = ((px - lx1) * (lx2 - lx1) + (py - ly1) * (ly2 - ly1) + (pz - lz1) * (lz2 - lz1)) / line_dist;
% %   t = constrain(t, 0, 1);
%   return dist_sq(px, py, pz, lx1 + t * (lx2 - lx1), ly1 + t * (ly2 - ly1), lz1 + t * (lz2 - lz1));
% }

end

