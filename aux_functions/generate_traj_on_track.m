function [trajectory_LLA] = generate_traj_on_track(start_LLA, distance, delta_s, tram_track)
%generate_traj_on_track Summary of this function goes here
%   Detailed explanation goes here

num_of_points = round(distance/delta_s);


trajectory_LLA = cell(2,num_of_points);

[~, curr_point] = project_point_on_railtrack(start_LLA, tram_track);

for i=1:num_of_points
    [~, next_point] = next_railtrack_point(tram_track, curr_point, delta_s);
    trajectory_LLA{2,i} = next_point;
    curr_point = next_point;
    trajectory_LLA{1,i} = i*delta_s;
end


