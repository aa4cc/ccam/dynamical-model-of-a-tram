function [start_node_index, projected_point_on_segment] = project_point_on_railtrack(curr_point, tram_track)
% project_point_on_railtrack: finds nearest point on line given by "tram_track" from point "curr_point".
%   return its index (same as index of start node of the line) and also
%   projection of point onto line (nearest point from "curr_point" on line)

% Find nearest node based on Euclidian distance.
track_size = numel(tram_track.ids);
distances_xy = LLA_points_distance(tram_track.coords,curr_point);
[~, index_of_nearest_node] = min(distances_xy);

% Find between which nodes "curr_point" is and projection.
% Cannot exceed array index.
if (index_of_nearest_node ~= 1 && index_of_nearest_node ~= track_size)
    % point can be somwhere in this interval: (i-1 ...... i ...... i+1)
    [start_node_index, projected_point_on_segment] = find_nearest_point_on_segment(curr_point, index_of_nearest_node-1, ....
        index_of_nearest_node+1, tram_track); 
elseif (index_of_nearest_node == 1)
    % First node is nearest
    aug_tram_track.ids = [tram_track.ids(end), tram_track.ids];
    aug_tram_track.coords = [tram_track.coords(:,end), tram_track.coords];
    aug_tram_track.lines = [create_interpol_line(tram_track.coords(:,end),tram_track.coords(:,1)), tram_track.lines];
    aug_tram_track.distances = [LLA_points_distance(tram_track.coords(:,end), tram_track.coords(:,1)), tram_track.distances];
    
    [aug_start_node_index, projected_point_on_segment] = find_nearest_point_on_segment(curr_point, 1, 3, aug_tram_track);
    if aug_start_node_index == 1
        % There is a last node at first position of aug_tram_track
        start_node_index = track_size;
    else
        start_node_index = aug_start_node_index - 1;
    end
else
    % Last node is nearest
    aug_tram_track.ids = [tram_track.ids, tram_track.ids(1)];
    aug_tram_track.coords = [tram_track.coords, tram_track.coords(:,1)];
    aug_tram_track.lines = [tram_track.lines, create_interpol_line(tram_track.coords(:,end),tram_track.coords(:,1))];
    aug_tram_track.distances = [tram_track.distances, LLA_points_distance(tram_track.coords(:,end), tram_track.coords(:,1))];
    
    [start_node_index, projected_point_on_segment] = find_nearest_point_on_segment(curr_point, index_of_nearest_node-1, ....
    index_of_nearest_node+1, aug_tram_track);
%     if aug_start_node_index == track_size + 1
%         % There is a first node at last position of aug_tram_track
%         start_node_index = 1;
%     else
%         start_node_index = aug_start_node_index;
%     end
end

end

