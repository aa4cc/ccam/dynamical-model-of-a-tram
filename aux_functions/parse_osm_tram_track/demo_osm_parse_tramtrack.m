clear;
clc;
%%
% This is demo how to extract tram track from OSM file and find particular
% tram track based on its name

disp('In case of an error, check correct path to data (.osm and elevation data).');

%% We first need to download .osm file
% - First option is to from https://www.openstreetmap.org/, select 'small' area
% and click to 'Export'
% - Second option is https://wiki.openstreetmap.org/wiki/Planet.osm (Not tested)
% - Third option is from here https://export.hotosm.org/en/v3/exports/new/describe

% Note following programs:
%   - Osmosis: https://wiki.openstreetmap.org/wiki/Osmosis 
%       - Keeping only tram-things. Post-processing is then much faster
%         --read-xml INPUT_FILE.osm --way-key-value keyValueList="railway.tram,railway.tram_stop" --used-node --write-xml OUTPUT_FILE.osm
%   - https://wiki.openstreetmap.org/wiki/Osmconvert 
%       - Convert data to .osm
%   - https://wiki.openstreetmap.org/wiki/Osmfilter#Tags_Filter
%       - Good to filter out things you do not need for transporation or
%       merging more files, for instance:

%% Download matlab library for .osm parsing (if you already do not have it):
%   - I used: https://github.com/johnyf/openstreetmap
%   - Note that you need to also download the dependencies
%   - Note: You need to download the function: 'xml2struct.m' from: https://www.mathworks.com/matlabcentral/fileexchange/28518-xml2struct
%       and rename it to 'xml2struct_fex28518.m'. Add the function into the
%       same folder as other files from the library

%% Try to parse some map:
openstreetmap_filename = './data/KN-VN-NR.osm';
% Note that if you download original .osm file, the tag value of the
% tramtrack name IS DIFFERENT. There are different symbols but matlab has
% problem with storing e.g. Czech symbols. So try to first find the
% tramtrack by looking to xml file. It should be something like:
% <tag k="name" v="Tram 6:
tram_name = 'Tram 6: Kub�nsk� n�mest� -> Palmovka';

% convert XML -> MATLAB struct. This might take some time. For this file,
% it takes around 20 seconds.
[parsed_osm, osm_xml] = parse_openstreetmap(openstreetmap_filename);

%% Now, parse the tram track using function created by me (Loi Do):
% ---- If you dont have altitude_struct, you can make constant altitude in whole map:
% altitude_struct = 250;

% ---- If you have altitude struct, it should be in .tif format
elevation_map_path = './data/dem_KN-VN-NR.tif';
elev_data = double(importdata(elevation_map_path));
% ---- IMPORTANT: parse_elev_data is quite stupid function. May need some 
offset = 1e-3*[0.8100, 1.100]; % Why? Just to aling two dataset. The error might be caused by wrong conversion
north = 50.09257905441916;
south = 50.07230200470836;
west = 14.40499486110664;
east = 14.452676093081509;
limits = [north, south, west, east];
altitude_struct = parse_elev_data(elev_data, limits, offset);

% Note that you have only those nodes of the track which are in the .osm file
[tram_track, tramstops] = parse_tram_network(tram_name,osm_xml, parsed_osm, altitude_struct);

%% Visualization (note that I made some changes to original functuon 'plot_way'):
fig = figure;
ax = axes('Parent', fig);
hold(ax, 'on');
pcolor(altitude_struct.x,altitude_struct.y,altitude_struct.data);
shading interp;
plot_way(ax, parsed_osm);
plot_tramstops_in_map(tramstops);



