function [distances] = LLA_points_distance(vector_points,curr_point)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
lon1 = curr_point(1);
lat1 = curr_point(2);

lon2 = vector_points(1,:);
lat2 = vector_points(2,:);

R = 6378.137; % Radius of earth in KM
dLat = lat2.* pi / 180 - lat1 * pi / 180;
dLon = lon2.* pi / 180 - lon1 * pi / 180;

a = sin(dLat/2).* sin(dLat/2) + ...
    cos(lat1 * pi / 180).* cos(lat2.* pi / 180) .* ...
    sin(dLon/2) .* sin(dLon/2);

c = 2 * atan2(sqrt(a), sqrt(1-a));
d = R * c;
distances = d * 1000; % meters
end

