function [tram_merge_track, tramstops] = parse_tram_network(tram_name,osm_xml, parsed_osm, altitude_struct)
%parse_tram_network: Select members:
%   type="way" and role="". This should represents tram tracks
%   and
%   type="node" and role="stop". This should represents tram stops

osm_relations = osm_xml.relation;
members = find_tram_network(tram_name,osm_relations);

num_of_members = size(members,2);
railtracks_id = cell(1,num_of_members);
tramstops_id = cell(1,num_of_members);

j = 1;
k = 1;
for i=1:num_of_members
    if strcmp(members{i}.Attributes.type, 'way') && isempty(members{i}.Attributes.role)
        railtracks_id{j} = str2double(members{i}.Attributes.ref);
        j = j+1;
    elseif ( strcmp(members{i}.Attributes.type, 'node') && ( strcmp(members{i}.Attributes.role, 'stop') || ...
            strcmp(members{i}.Attributes.role, 'stop_entry_only') || ...
            strcmp(members{i}.Attributes.role, 'stop_exit_only')))
        tramstops_id{k} = str2double(members{i}.Attributes.ref);
        k = k+1;
    end
end

% Remove empty
railtracks_id = railtracks_id(~cellfun('isempty',railtracks_id));
tramstops_id = tramstops_id(~cellfun('isempty',tramstops_id));

% Tram stops
tramstops = cell(3,size(tramstops_id,2));
tramstops(1,:) = tramstops_id;
tramstops(2:3,:) = find_tramstop_atributes(tramstops_id, osm_xml);

% extend 1 dim cell to 5 dim cell to save another information
% 1- ID of nodes,
% 2- ID of the ways,
% 3- Coords of nodes,
% 4- Parametrization of line between nodes 
% 5- relative distances between nodes

size_of_track = size(railtracks_id,2);
tram_railtrack = cell(5, size_of_track);
tram_railtrack(1,:) = railtracks_id;

% For every way in network
for i=1:size_of_track
    [curr_nodes, xy] = find_way_nodes(tram_railtrack{1,i}, parsed_osm);
    if (not(isempty(curr_nodes)))
        tram_railtrack{2,i} = curr_nodes{1};
        tram_railtrack{3,i} = xy;
    end
end

% Compute lines for interpolation between nodes.
for i=1:size_of_track
    num_of_lines = size(tram_railtrack{3,i},2)-1;
    lines = zeros(2,num_of_lines); % interpolation lines
    distances = zeros(1,num_of_lines); % distances of nodes
    for j=1:num_of_lines
        coord_1 = tram_railtrack{3,i}(:,j);
        coord_2 = tram_railtrack{3,i}(:,j+1);
        lines(:,j) = create_interpol_line(coord_1, coord_2);
        distances(j) = LLA_points_distance(coord_1,coord_2);
    end

    tram_railtrack{4,i} = lines;
    tram_railtrack{5,i} = distances;
end
% Merge railwayss, select only non-empty:
tram_merge_track = merge_ways(tram_railtrack);
tram_merge_track = add_nodes_altitude(tram_merge_track, altitude_struct);
tram_merge_track = add_nodes_angles(tram_merge_track);

end


