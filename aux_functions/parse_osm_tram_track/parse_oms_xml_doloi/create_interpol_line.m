function [X] = create_interpol_line(coord_1,coord_2)
%create_interpol_line: Return analytic description of line between two
%coordinates: coord_1 and coord_2 as Ax + B = y
%   Return X, where X(1) is A and X(B) is B.
A = [coord_1(1) 1;
     coord_2(1) 1];
B = [coord_1(2);coord_2(2)];
X = linsolve(A,B);
end

