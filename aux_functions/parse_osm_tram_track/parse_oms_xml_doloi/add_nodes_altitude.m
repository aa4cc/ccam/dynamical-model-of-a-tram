function tram_track_out = add_nodes_altitude(tram_track,altitude_struct)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

num_of_samples = size(tram_track.ids,2);
altitude = zeros(1,num_of_samples);

if(isstruct(altitude_struct))
    smooth_elev_data = ordfilt2(altitude_struct.data,1,ones(3,3));
    for i=1:num_of_samples
        [~,x_index] = min(abs(altitude_struct.x - tram_track.coords(1,i)));
        [~,y_index] = min(abs(altitude_struct.y - tram_track.coords(2,i)));
        altitude(i) = smooth_elev_data(y_index, x_index);
    end
    
    altitude(altitude == 0) = altitude(find(altitude,1,'last'));
    altitude = medfilt1(altitude, 20);
else
    % Altitude is only one number
    altitude(:,:) = altitude_struct;
end
    tram_track_out = tram_track;
    tram_track_out.altitude = altitude;
end

