function [nd_coords] = find_nodes_coords(nodes_id,parsed_osm)
%find_nodes_coords: Finds coordinates as (Latitude Longitude) form ID of
%nodes.
%   From ID of nodes (vector) and parsed osm, return array of coordinates,
%   for each node.
num_nd = size(nodes_id{1}, 2);
nd_coords = zeros(2, num_nd);
node = parsed_osm.node;
for i=1:num_nd 
    cur_nd_id = nodes_id{1}(i);
    if ~isempty(node.xy(:, cur_nd_id == nodes_id{1}))         % take coordinates(column) of map-point if equal to current ID path-point.
        nd_coords(:, i) = node.xy(:, cur_nd_id == parsed_osm.node.id);   % coordinates of path-points.
    end
end
end

