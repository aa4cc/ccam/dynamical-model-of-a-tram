function [nodes_id, nodes_xy] = find_way_nodes(ways_id, parsed_osm)
%find_way_nodes: Find nodes from which ways are constructed
%   Take ID of ways and parsed osm, For each way, find its nodes.
%   Return array of IDs of nodes and array of coordinates of nodes.
nodes_id = {};
nodes_xy = {};

% for all ways in parsed_osm
for i=1:size(parsed_osm.way.id,2)
    if(parsed_osm.way.id(i) == ways_id)
        nodes_id = parsed_osm.way.nd(i);
        nodes_xy = find_nodes_coords(nodes_id,parsed_osm);
        break;
    end
end
end

