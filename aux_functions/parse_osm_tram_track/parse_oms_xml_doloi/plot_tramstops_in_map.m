function done = plot_tramstops_in_map(tramstops)
%plot_tramstops_in_map Summary of this function goes here
%   Detailed explanation goes here

% Find where we have LLA coords for tramstops
indexes_of_tramstops = find(~cellfun(@isempty,tramstops(3,:)));

for i=1:size(indexes_of_tramstops, 2)
    x = tramstops{3,indexes_of_tramstops(i)}(1);
    y = tramstops{3,indexes_of_tramstops(i)}(2);
    plot(x, y,'-k*','markers',5);
    desctiption = tramstops{2,indexes_of_tramstops(i)};
    text(x,y, desctiption, 'VerticalAlignment','top', 'HorizontalAlignment','left')
end
done = 0;
end

