function elev_struct = parse_elev_data(elev_data, limits, offset)
% elev_data = double(geoimread(elevation_map_path));

%after offset:
north = limits(1)-offset(1);
south = limits(2)-offset(1);
west = limits(3)-offset(2);
east = limits(4)-offset(2);

x = linspace(west, east,size(elev_data, 2));
y = linspace(north, south,size(elev_data, 1));

elev_struct.x = x;
elev_struct.y = y;
elev_struct.data = elev_data;
end

