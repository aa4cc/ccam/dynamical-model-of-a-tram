function [index, end_point] = next_railtrack_point(railtrack,start_point,distance)
%START FUNCTION: next_railtrack_point:
% Finds next point on tram trail given by starting
% point (LLA) and traveled distance (metres)

% get projection of start point on track and get index of starting node
[index, p_start] = project_point_on_railtrack(start_point, railtrack);

found = false;
temp_dist = 0;
curr_point = p_start;

% Find start node of segment, on which end_point lies
track_size = numel(railtrack.ids);

if (distance > 0)
    while (~found)
        % compute distance to end of current segment
        prev_distance = temp_dist;
        
        % Get coords of point at the end of current segment. Mod() for
        % round track so last node is connected to first.
        next_point = railtrack.coords(:,mod(index, track_size)+1);
        temp_dist = temp_dist + LLA_points_distance(next_point,curr_point);
        
        if (temp_dist < distance)
            % next iter init, end point is after 'next_point'
            curr_point = next_point;
            index = index + 1;
        else
            % Exceed distance, final point is between "curr_point" and "next_point"
            found = true;
            % Compute how much to travel from curr_point to end_point; Both
            % points are on the same line.
            dist_last_segment = distance - prev_distance;
            last_segment_dist = LLA_points_distance(curr_point, next_point);
            ratio = dist_last_segment/last_segment_dist;
            end_point = curr_point + ratio*(next_point - curr_point);
        end
    end
else
    distance = -distance;
    while (~found)
        prev_distance = temp_dist;
        prev_point = railtrack.coords(:,index);
        % compute distance to end of current segment
        temp_dist = temp_dist + LLA_points_distance(prev_point,curr_point);
        
        if (temp_dist < distance)
            % next iter init
            curr_point = prev_point;
            index = index - 1;
            if (index == 0) % To prevent underflow, for round tracks.
                index = numel(railtrack.ids);
            end
        else
            % Exceed distance, final point is between "curr_point" and "prev_point"
            found = true;
            % Compute how much to travel from curr_point to end_point;
            dist_last_segment = distance - prev_distance;
            last_segment_dist = LLA_points_distance(curr_point, prev_point);
            ratio = dist_last_segment/last_segment_dist;
            end_point = prev_point + (1-ratio)*(curr_point - prev_point);
        end
    end
end
% END FUNCTION: next_railtrack_point
end

