function [start_node_index,projected_point_on_segment] = approximate_nearest_point(curr_point, start_index, end_index,tram_track)

start_node_index = 0;
previous_i = start_index;
previous_point = tram_track.coords(:,start_index);

for i = start_index:end_index-1
    start_point = tram_track.coords(:,i);
    end_point = tram_track.coords(:,i+1);
    
    distance = tram_track.distances(i);
    previous_distance = LLA_points_distance([start_point(1) ; start_point(2)] ,curr_point);
    interval = linspace(start_point(1), end_point(1), round(distance/0.01));
    for j=interval(2:end)
        x = j;
        y = tram_track.lines(1,i)*x + tram_track.lines(2,i);
        
        current_distance = LLA_points_distance([x ; y] ,curr_point);
        if (current_distance > previous_distance)
            start_node_index = previous_i;
            projected_point_on_segment = previous_point;
            break;
        end
        previous_distance = current_distance;
        previous_point = [x ; y];
        previous_i = i;
    end
    if (start_node_index ~= 0)
        break;
    else
        % If nearest point is end of the tram network
        start_node_index = end_index;
        projected_point_on_segment = tram_track.coords(:,end_index);
    end
end
end

