function done = display_lla_points(lla_data, display_time_period, data_timestep)
% display_lla_points Display points in LLA on map

num_of_sets = size(lla_data,2);
timestamps_all_data = zeros(1,num_of_sets);
h_points = cell(1,num_of_sets);

% From all datasets, finds highest time
for i=1:num_of_sets
    lla_data{i}.time = (lla_data{i}.time);
    timestamps_all_data(i) = lla_data{i}.time(end);
end
end_time = max(timestamps_all_data);

% Main time loop, display every 0.01 sec
time_step = data_timestep;
first_plot(1:num_of_sets) = true;
colors = {'*g', '*b', '*k', '*c', '*m', '*y', '*r'};

for time=linspace(0.2,end_time, end_time/time_step)
    disp(time);
    for i=1:num_of_sets
        curr_point = lla_data{i}.data(:, find(lla_data{i}.time<=time,1,'last') );
        if(~isempty(curr_point))
            if (~first_plot(i))
                % Redraw
                set(h_points{i},'XData',curr_point(1),'YData',curr_point(2));
            else
                % First plot
                h_points{i} = plot(curr_point(1),curr_point(2), colors{i});
                first_plot(i) = false;
            end
        end
    end
    pause(display_time_period);
end
done = 1;
end










