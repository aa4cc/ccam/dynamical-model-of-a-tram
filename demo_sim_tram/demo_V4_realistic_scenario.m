%% Simulation of tram's dynamics
% - Tatra T3 model
% - Digital map included
% - Measurements without noise

clc;
clear;
close all;

%% Parse Digital map
% You might want to save the "tram_track", "tramstops", "altitude_struct",
% "parsed_osm",
% into a .mat file so the parsing is not done every single time

is_map_saved = 1;
if is_map_saved == 0
    openstreetmap_filename = './data/KN-VN-NR.osm';
    tram_name = 'Tram 6: Kubánské námestí -> Palmovka';
    [parsed_osm, osm_xml] = parse_openstreetmap(openstreetmap_filename);
    elevation_map_path = './data/dem_KN-VN-NR.tif';
    elev_data = double(importdata(elevation_map_path));
    offset = 1e-3*[0.8100, 1.100]; % Offset to align the two maps. The error might be caused by wrong conversion
    north = 50.09257905441916;
    south = 50.07230200470836;
    west = 14.40499486110664;
    east = 14.452676093081509;
    limits = [north, south, west, east];
    altitude_struct = parse_elev_data(elev_data, limits, offset);
    [tram_track, tramstops] = parse_tram_network(tram_name,osm_xml, parsed_osm, altitude_struct);
    for i=1:4
        % Add info of distance of tramstops
        tramstops{4,16+i} = railtrack_distance_not_round(tram_track, tramstops{3,16}, tramstops{3,16+i});
    end
else
    load('.\data\digital_map_KN-NR_tram6.mat');
end


%% Simulation setup
sim_time = 430;
sim_Ts = 0.001;

% -----------------------------------------
% ----------- Select tram model -----------
% -----------------------------------------
model_name = 'tatra_t3_model';


% --------------------------------------------------------------------
% ----------- Define input signals / (physical) parameters -----------
% --------------------------------------------------------------------

% ----- Initial values -----
vel_init = 10;  % m/s
start_pos_LLA = tramstops{3,16}; % Geographic starting position of the tram. (Tramstop)

% ------------------------------------------------------------
% ----- Weight-change signal for simulink "To Workspace" -----
change_weight_times =  [0,  100,   210,     315,    sim_time]; % (1)==0, (end)==sim_time: lenght must be +1 of change_weight_values
change_weight_values = [17000, 21000, 18000, 25000];
weight_sig = create_from_workspace_signal(change_weight_values,change_weight_times, sim_Ts, sim_time);


% -------------------------------------
% ----- Distance reference signal -----
change_dist_times = [0, 5,   118,     218,    320,  sim_time];
change_dist_values = [0, cell2mat(tramstops(4,16:20))];
reference_dist_sig = create_from_workspace_signal(change_dist_values,change_dist_times, sim_Ts, sim_time);


% % ----------------------------------------------------------------
% % ----- Notch signal (CZ: pozice ridici paky urcena ridicem) -----
% %  Set by the driver: integer, should be in the interval <-7, -6, .., 6, 7>.
% change_notch_times = [00, 10, 15, 30, 45, 50, 55, sim_time];
% change_notch_values =  [00, 02, 00, -7, 00, 01, 00];
% notch_sig = create_from_workspace_signal(change_notch_values,change_notch_times, sim_Ts, sim_time);

% ----------------------------------------------------------------
% ----- Slope-change look-up table data for simulink -----
% Given by the digital map, Create slope_dist and slope_rad vectors for 1-D loop-up table in Simulink
is_elevation_saved = 0;
if is_elevation_saved == 1
    load('.\data\digital_elevation_KN-NR.mat');
else
    tot_dist = 1601; % Select any reasonable value to cover your whole distance.
    delta_dist = 1;
    gen_traj = generate_traj_on_track(start_pos_LLA,tot_dist,delta_dist,tram_track); % Generate 'equidistant' trajectory
    sin_slope_traj = movmean(travel_slope(gen_traj, tram_track, 20,20),20); % numerical values at the end are selected as "filter" coefficients
    data_mat_traj = cell2mat(gen_traj);
    slope_dist = data_mat_traj(1,:);
    slope_rad = asin(sin_slope_traj);
    
end
% plot(slope_dist, slope_rad);


% -----------------------------------------------------
% ----- Curvature look-up table data for simulink -----
is_curvature_saved = 1;
if is_curvature_saved == 1
    load('.\data\digital_map_KN-NR_curv.mat');
else
    tot_dist = 1601; % Select any reasonable value to cover your whole distance.
    delta_dist = 1;
    mov_mean_k = 10;
    distance_ahead = 15;
    gen_traj = generate_traj_on_track(start_pos_LLA,tot_dist,delta_dist,tram_track); % Generate 'equidistant' trajectory
    data_mat_traj = cell2mat(gen_traj);
    curv_dist = data_mat_traj(1,:);
    curvature = movmean(travel_curv(gen_traj, tram_track, distance_ahead, distance_ahead),mov_mean_k);
    
end
% plot(curv_dist, curvature);



% -------------------------------
% ----- Adhesion conditions -----
adh_cond = 0;   % 0: dry, 1: wet1, 2: wet2, 3: wet3



%% Simulation
model_path = ['.\tram_simulink_models\', model_name, '.slx'];
eval([model_name, '_setup']);

uiopen(model_path,1);
simOut = sim(model_name,'StartTime','0','StopTime',num2str(sim_time),'FixedStep',num2str(sim_Ts));
sim_out_time = simOut.logsout.get('tram_distance').Values.Time;
sim_out_dist = simOut.logsout.get('tram_distance').Values.Data;
sim_out_speed = simOut.logsout.get('tram_speed').Values.Data;
sim_out_acc = simOut.logsout.get('tram_acceleration').Values.Data;
sim_out_acc_biased_grav = simOut.logsout.get('tram_acceleration_gravity').Values.Data;
sim_out_gyro_z = simOut.logsout.get('tram_gyroscope').Values.Data;
sim_out_acc_y = simOut.logsout.get('tram_acc_y').Values.Data;

%% Generate sensor measurements
state.dist = sim_out_dist;
state.Ts = sim_Ts;
state.acc = sim_out_acc_biased_grav;
state.vel = sim_out_speed;
state.time = sim_out_time;
state.gyro_z = sim_out_gyro_z;
state.acc_y = sim_out_acc_y;
sensor_Ts = 0.1;

[gps_pos_data, gps_vel, acc_x, acc_y, gyro_z]  = gen_sensor_data_v2_on_track(start_pos_LLA, state, sensor_Ts, tram_track);


%% Plot the results
figure;
plot(sim_out_time, sim_out_dist);
grid on;
hold on;
plot(reference_dist_sig.time, reference_dist_sig.signals.values);
title('Simulated traveled distance');
legend('Simulated traveled distance', 'Reference');

figure;
plot(gps_vel.time, gps_vel.data);
grid on; 
hold on;
title('Simulated speed with noise');

figure;
plot(acc_x.time, acc_x.data);
grid on;
hold on;
title('Simulated acceleration with noise');

figure;
plot(acc_y.time, acc_y.data);
hold on;
plot(gyro_z.time, gyro_z.data);
legend('Acceleration Y', 'Gyroscope Z')

%% Display map and GPS data
% Visualization
fig = figure;
ax = axes('Parent', fig);
hold(ax, 'on');
pcolor(altitude_struct.x,altitude_struct.y,altitude_struct.data);
shading interp;
plot_way(ax, parsed_osm);
plot_tramstops_in_map(tramstops);


lla_data{1} = gps_pos_data;
display_time_period = 0.1;
data_timestep = 1;
display_lla_points(lla_data, display_time_period, data_timestep);







