%% Simulation of tram's dynamics
% - Tatra T3 model
% - Constant parameters and conditions (No digital map)
% - Measurements without noise

clc;
clear;
close all;

%% Simulation setup
sim_time = 200;
sim_Ts = 0.001;

vel_init = 10;  % m/s
adh_cond = 0;   % 0: dry, 1: wet1, 2: wet2, 3: wet3
tram_weight = 17000;


% Select tram model
model_name = 'tatra_t3_model';

model_path = ['.\tram_simulink_models\', model_name, '.slx'];
eval([model_name, '_setup']);
uiopen(model_path,1);

% Select parameters
change_weight_times =  [0, sim_time]; % Last time must be the sim_time. The array define bounds of interval for "change" values
change_weight_values = tram_weight;
weight_sig = create_from_workspace_signal(change_weight_values,change_weight_times, sim_Ts, sim_time);

% -------------------------------------
% ----- Distance reference signal -----
change_dist_times = [0, sim_time];
change_dist_values = [1000]; 
reference_dist_sig = create_from_workspace_signal(change_dist_values,change_dist_times, sim_Ts, sim_time);




% ----------------------------------------------------------------
% ----- Notch signal (CZ: pozice ridici paky urcena ridicem) -----
%  Set by the driver: integer, should be in the interval <-7, -6, .., 6, 7>. 
%%% Commented out because notch is set by distance reference %%%
% change_notch_times = [0, sim_time];
% change_notch_values =  -6; 
% notch_sig = create_from_workspace_signal(change_notch_values,change_notch_times, sim_Ts, sim_time);

% AUX variables
slope_dist = 1:2; 
curv_dist = 1:2;
slope_rad = zeros(2,1); % Assume zero slope/inclination of the track
sin_slope_traj = zeros(2,1); 
curvature = zeros(2,1); 


simOut = sim(model_name,'StartTime','0','StopTime',num2str(sim_time),'FixedStep',num2str(sim_Ts));
sim_out_time = simOut.logsout.get('tram_distance').Values.Time;
sim_out_dist = simOut.logsout.get('tram_distance').Values.Data;
sim_out_speed = simOut.logsout.get('tram_speed').Values.Data;
sim_out_acc = simOut.logsout.get('tram_acceleration').Values.Data;

%% Plot the results
figure;
plot(sim_out_time, sim_out_dist);
grid on;
hold on;
title('Simulated travelled distance');

figure;
plot(sim_out_time, sim_out_speed);
grid on;
hold on;
title('Simulated speed');

figure;
plot(sim_out_time, sim_out_acc);
grid on; 
hold on;
title('Simulated acceleration');
